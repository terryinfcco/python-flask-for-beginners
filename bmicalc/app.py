from flask import Flask, render_template, request

# Create the app using standard terminology
app = Flask(__name__)

# decorator to define the index page
@app.route('/', methods=['GET', 'POST'])

def welcome():
    height = ''
    weight = ''
    bmi = 0
    if request.method == "POST" and "userheight" in request.form:
        height = float(request.form.get("userheight"))
        weight = float(request.form.get("userweight"))
        bmi = round(weight / (height**2) * 703 ,1)
    return render_template('index.html', bmi=bmi)

# at the very end of the app script
app.run(debug=True)