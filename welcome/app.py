from flask import Flask, render_template, request

# Create the app using standard terminology
app = Flask(__name__)

# decorator to define the index page
@app.route('/', methods=['GET', 'POST'])

def welcome():
    name = ''
    food = ''
    if request.method == "POST" and "username" in request.form:
        name = request.form.get("username")
        food = request.form.get("userfood")
    return render_template('index.html', name=name, food=food)

# at the very end of the app script
app.run(debug=True)